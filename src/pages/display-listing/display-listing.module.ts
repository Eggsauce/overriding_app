import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DisplayListingPage } from './display-listing';

@NgModule({
  declarations: [
    DisplayListingPage,
  ],
  imports: [
    IonicPageModule.forChild(DisplayListingPage),
  ],
})
export class DisplayListingPageModule {}
