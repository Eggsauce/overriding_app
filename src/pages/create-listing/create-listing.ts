import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, DateTime } from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { RequestOptions, Http, Response, Headers, RequestMethod } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ListingModel } from '../../models/listingmodel';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { count } from 'rxjs/operators';
import { state } from '@angular/core/src/animation/dsl';
import { iterateListLike } from '@angular/core/src/change_detection/change_detection_util';

/**
 * Generated class for the CreateListingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-listing',
  templateUrl: 'create-listing.html',
})
export class CreateListingPage {

  /* #region Declare listins and called out From Model Class*/
  listings: ListingModel[] = [];
 /* #endregion */
 /* #region Declare the value pass from home page */
  agent: string;
  agentContact: string;
  /* #endregion */

  /*#region Declare text box value [ngModel]*/
  AdsTitle: string;
  unitnumber: string;
  ownername: string;
  owneremail: string;
  ownercontact: string;
  ownercontact2: string;
  propertyaddress: string;
  state: any;
  area: any;
  listingtype: string;
  tenure: string;
  title: string;
  listingprice: Float32Array;
  room: number;
  bathroom: number;
  carpark: number;
  other: string;
  description: string;
  remark: string;
  image: string;
  /* #endregion */
/* #region Dropdown Declare */
  
  //Declare a variable that is observable any to store the json value
  listingtypes: Observable<any>;
  tenures: Observable<any>;
  titles: Observable<any>;
  others: Observable<any>;
  propertytypes: Observable<any>;
  
  //declare variable that store any is to store the dropdown value from json object.
  public states: any[];
  public areas: any[];
  public selectedArea: any[];

  
   
 
/* #endregion */
 





constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public http:Http, 
    private ApiService: ApiserviceProvider) {

     
   
      /* #region Load Dropdown */
      //Load all the method called out from api into onload page display the dropdown.
      this.loadPropertyType();
      this.loadState();
      this.loadArea();
      this.loadListingType();
      this.loadTenure();
      this.loadTitle();
      this.loadOthers();
      /* #endregion */
 
     
  }
  
  
 
  
/* #region Create Listing Method */
  create(){
    
    this.agent = this.navParams.get('agent');
    this.agentContact = this.navParams.get('agentContact');

    //Declare a constant model class 
    const newListing: ListingModel = new ListingModel();

    //Pass value to model
    newListing.AdsTitle = this.AdsTitle;
    newListing.UnitNo = this.unitnumber;
    newListing.Agent = this.agent;
    newListing.AgentContact = this.agentContact;
    newListing.OwnerName = this.ownername;
    newListing.OwnerEmail = this.owneremail;
    newListing.OwnerContact = this.ownercontact;
    newListing.OwnerContact2 = this.ownercontact2;
    newListing.Address = this.propertyaddress;
    newListing.State = this.state.StateName;
    newListing.Area = this.area.AreaName;
    newListing.ListingType = this.listingtype;
    newListing.Tenure = this.tenure;
    newListing.Title = this.title;
    newListing.ListingPrice = this.listingprice;
    newListing.Room = this.room;
    newListing.Bathroom = this.bathroom;
    newListing.Carpark = this.carpark;
    newListing.Description = this.description;
    newListing.Other = this.other;
    newListing.Remarks = this.remark;
    let  Status = newListing.Status ? parseInt(newListing.Status): -1;
    newListing.Status = Status;
    

    /*take all the "newListing" that store the textbox value and use the addListing method from api service provider to subscribe
    the api and add the listing into it.*/
    this.ApiService.addlisting(newListing)
    .subscribe(insertedListing => {
      this.listings.push(insertedListing);
    });


  
  }

  /* #endregion */

 
/* #region Retrieve API For Dropdown */
//get Property Type Dropdown
  loadPropertyType()
  {
    
    this.ApiService.getPropertyType().subscribe((res: Response) => {
      const propertytype = res.json();
      this.propertytypes = propertytype;
    })

  }

  //get State Dropdown
  //Refer api from APIService Provider
  // load the state method
 loadState()
 {
   //Retrieve getState Method from ApiService Provider to execute the api.
    //Called the api service method in here and using subsribe method to get the api response.
   this.ApiService.getState().subscribe((res: Response) =>
   {
     //Declare a const name state to retrieve the api json object.
     const state = res.json();
      //Assign the followin data into an observable name states that has declare above.
     this.states = state;
   })
 }

 

  //get Area Dropdown
   //Refer api from APIService Provider
  // load the area method
  loadArea()
 {
   //Retrieve getArea Method from ApiService Provider to execute the api.
    //Called the api service method in here and using subsribe method to get the api response.
   this.ApiService.getArea().subscribe((res: Response) =>
   {
     //Declare a const name area to retrieve the api json object.
     const area = res.json();
     //Assign the followin data into an observable name states that has declare above.
     this.areas = area;
   })
 }

  //get Listing Type
  //Refer api from APIService Provider
  // load the listing type method
  loadListingType()
  {
     //Retrieve getListingType Method from ApiService Provider to execute the api.
    //Called the api service method in here and using subsribe method to get the api response.
  this.ApiService.getListingType().subscribe((res: Response) => {

    //Declare a const name listingtype to retrieve the api json object.
    const listingtype = res.json();
    //Assign the followin data into an observable name states that has declare above.
    this.listingtypes = listingtype; 
  })
  }

  //get Tenure
  //Refer api from APIService Provider
  // load the tenure method
  loadTenure()
  {  
    //Retrieve getTenure Method from ApiService Provider to execute the api.
    //Called the api service method in here and using subsribe method to get the api response.
  this.ApiService.getTenure().subscribe((res: Response) => {

    //Declare a const name tenure to retrieve the api json object.
    const tenure = res.json();
    //Assign the followin data into an observable name states that has declare above.
    this.tenures = tenure;
  })

  }
  
   //get Title
   //Refer api from APIService Provider
  // load the title method
  loadTitle()
  {
   //Retrieve getTitle Method from ApiService Provider to execute the api.
    //Called the api service method in here and using subsribe method to get the api response.
  this.ApiService.getTitle().subscribe((res:Response) => {
    //Declare a const name title to retrieve the api json object.
    const title = res.json();
    //Assign the followin data into an observable name states that has declare above.
    this.titles = title;
    
  })
  }

  //get Others
  //Refer api from APIService Provider
  // load the others method
  loadOthers()
  {
    //Retrieve getOthers Method from ApiService Provider to execute the api.
    //Called the api service method in here and using subsribe method to get the api response.
  this.ApiService.getOthers().subscribe((res: Response) => {

    //Declare a const name other to retrieve the api json object.
    const other = res.json();
    //Assign the followin data into an observable name states that has declare above.
    this.others = other;
  })
  }
  /*#endregion*/
  
  
  //Cascading Select For State & Area
  stateChange(state) {
    /*use the selectedArea variable that declare above that use to store all the areas result that is filter by StateID in Area_TB
    and StateID in State_TB*/
    this.selectedArea = this.areas.filter(area => area.StateID == state.StateID);
    
  }
  
  
  

  
  
 
}


  
    

