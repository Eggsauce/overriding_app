import { Component } from '@angular/core';
import { NavController, NavParams, Loading } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import { WelcomePage } from '../welcome/welcome';
import { Http,Response } from '@angular/http';
import { SponsorDetailPage } from '../sponsor-detail/sponsor-detail';
import { CreateListingPage } from '../create-listing/create-listing';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';
import { DisplayListingPage } from '../display-listing/display-listing';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
 //Declare an array that store 'any' value meaning it can be any data type name result.
  result: any[]=[];
  data: Observable<any>;

  //Declare user and pass as string for get the agent userid and pass from login page and to pass the value to other page.
  user: string;
  pass: string;

  //Declare agent detail from ngModel so that it can able to called out data in Home Page.
  FirstName = '';
  LastName = '';
  UserID = '';
  UserEmail ='';
  BankName = '';
  MobileCountry = '';
  MobileNo ='';
  
  mobile: string;
 
  constructor(public navCtrl: NavController,
               public navParams: NavParams, 
               public http: HttpClient,
               public https:Http,
               public ApiService: ApiserviceProvider) {

                //Get the value using navParams from login page after login.
                this.user = navParams.get('data');
                this.pass = navParams.get('data1');

                
              //Execute the loadUserDetail Method in OnLoad section.
                this.loadUserDetail();
    
   
  
    
    
  }

//Refer api from APIService Provider
// load the user detail method
  loadUserDetail()
  {
    //Retrieve DisplayUser Method from ApiService Provider to execute the api.
    //Called the api service method in here and using subsribe method to get the api response.
    this.ApiService.DisplayUser(this.user, this.pass)
    .subscribe((res: Response) => {
    
      //Declare a const name getUser to retrieve the api json object.
      const getUser = res.json();
      //Assign the followin data into an array name result that has declare above.
      this.result = getUser.u;
      //Assign the mobile country and mobile no into mobile to combine the mobile number.
      this.mobile = getUser.u.MobileCountry + getUser.u.MobileNo;
      
    })
  }
  
//Redirect Sponsors Detail method and pass the user & pass data to allow api to get the data using navParams to get the data.
  redirectToSponsor(){
   
    //Using the navCtrl to redirect to Sponsor Detail Page
    this.navCtrl.push(SponsorDetailPage, {
      //assign the user and pass value that comes from login to the following variable and push to Sponsor Detail Page.
      data: this.user,
      data1: this.pass
    });
  }
 

 logout(){
   this.navCtrl.push(WelcomePage);
 }

 //Redirect Listing method and pass the user & mobile num data to allow api to get the data using navParams to get the data.
 redirectToListing()
 {
   //Using the navCtrl to redirect to Create Listing Page
   this.navCtrl.push(CreateListingPage,{

    //assign the user and mobile value that comes from onload to the following variable and push to Create Listing Page.
    agent: this.user,
    agentContact: this.mobile
    
   });

 }

 //Redirect Display Listing method 
 redirectToListingDetail()
 {
   this.navCtrl.push(DisplayListingPage);
 }
 
}