import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfitModalPage } from './profit-modal';

@NgModule({
  declarations: [
    ProfitModalPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfitModalPage),
  ],
})
export class ProfitModalPageModule {}
