import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Http,Response } from '@angular/http';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';


/**
 * Generated class for the SponsorDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sponsor-detail',
  templateUrl: 'sponsor-detail.html',
})
export class SponsorDetailPage {

  //Declare user and pass to accept the value that comes from home page
  user: string;
  pass: string;

  //Declare an array that store the sponsor data name result
  result: any[]=[];
  constructor(public navCtrl: NavController,
             public navParams: NavParams, 
             public https: Http,
             public ApiService: ApiserviceProvider,
             public modal: ModalController) {
    //value comes from home page.
    this.user = navParams.get('data');
    this.pass = navParams.get('data1');

    //Method called in onload to load the detail
    this.loadSponsorDetail();
    
  
  }

//Refer api from APIService Provider
// load the sponsor detail method
  loadSponsorDetail()
  {
    //Retrieve DisplaySponsorDetail Method from ApiService Provider to execute the api.
    //Called the api service method in here and using subsribe method to get the api response.
    this.ApiService.DisplaySponsorDetail(this.user, this.pass)
    .subscribe((res: Response) => {

      //Declare a const name getSponsors to retrieve the api json object.
      const getSponsors = res.json();
      //Assign the followin data into an array name result that has declare above.
     this.result = getSponsors.u;

    })
  }

  //A Modal Method using Modal Controller
  openDirectSponsorModal()
  {
    //Declare a const that will that will store the ready create modal page into it.
    const directSponsorModal = this.modal.create('DirectSponsorModalPage',
    {
      //At the same time pass the user & pass to next following page to able to use user and pass called out from api
      data: this.user,
      data1: this.pass
    });
    //Using the const variable to present the Modal Popup
    directSponsorModal.present();
  }

  //A Modal Method using Modal Controller
  openSponsorModal()
  {
     //Declare a const that will that will store the ready create modal page into it.
    const sponsormodal = this.modal.create('SponsorModalPage',{
      //At the same time pass the user & pass to next following page to able to use user and pass called out from api
      data: this.user,
      data1: this.pass
    });
     //Using the const variable to present the Modal Popup
    sponsormodal.present();
  }

  //A Modal Method using Modal Controller
  openSalesModal()
  {
    //Declare a const that will that will store the ready create modal page into it.
    const salesmodal = this.modal.create('SalesModalPage',{
       //At the same time pass the user & pass to next following page to able to use user and pass called out from api
      data: this.user,
      data1: this.pass
    });
     //Using the const variable to present the Modal Popup
    salesmodal.present();
  }

  //A Modal Method using Modal Controller
  openProfitModal()
  {
    //Declare a const that will that will store the ready create modal page into it.
    const profitmodal = this.modal.create('ProfitModalPage',{
      //At the same time pass the user & pass to next following page to able to use user and pass called out from api
      data: this.user,
      data1: this.pass
    });
     //Using the const variable to present the Modal Popup
    profitmodal.present();
  }

  
  

}
