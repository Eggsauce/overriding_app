import { DateTime } from "ionic-angular";

//Declare all the value in this model can so that other page can using this model class to add or edit data from the api.
export class ListingModel {
    Id: string;
    AdsTitle: string;
    Photo: string;
    Url: null;
    Address: string;
    Measurement: string;
    UnitNo: string;
    Tenure: string;
    Title: string;
    PropertyType: string;
    ListingType: string;
    Condition: string;
    Description: string;
    AddedDate: DateTime;
    Status: any;
    State: string;
    Area: string;
    Agent: string;
    AgentContact: string;
    OwnerName: string;
    OwnerContact: string;
    OwnerEmail: string;
    Room: number;
    Bathroom: number;
    Carpark: number;
    Maintenance: number;
    Sinking: number;
    BuildUp: string;
    LandArea: string;
    CompletionDate: string = new Date().toLocaleDateString();
    Remarks: string;
    ListingPrice: Float32Array;
    Other: string;
    ApproveBy: string;
    OwnerContact2: string;
    ListingSale: Float32Array;
    UnitStatus: string;
    Measurement_L: string;
}