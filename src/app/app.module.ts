import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { WelcomePage } from '../pages/welcome/welcome';
import { LoginPage } from '../pages/login/login';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import {HttpClientModule} from '@angular/common/http'
import { HttpModule } from '@angular/http'

import { SponsorDetailPage } from '../pages/sponsor-detail/sponsor-detail';
import { CreateListingPage } from '../pages/create-listing/create-listing';
import { ApiserviceProvider} from '../providers/apiservice/apiservice';
import { Camera } from '@ionic-native/camera';
import { DisplayListingPage } from '../pages/display-listing/display-listing';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    WelcomePage,
    LoginPage,
    SponsorDetailPage,
    CreateListingPage,
    DisplayListingPage
 
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    WelcomePage,
    LoginPage,
    SponsorDetailPage,
    CreateListingPage,
    DisplayListingPage
  
   
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler}, 
    ApiserviceProvider
  ]
})
export class AppModule {}
