import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ListingModel } from '../../models/listingmodel';
import { Observable } from 'rxjs';
import { catchError,tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Http, Response } from '@angular/http';


/*
  Generated class for the ListingServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
//Declare a httpOptions and mention the header in json format so that HTTP Post method can use this option.
const httpOptions = {
  headers : new HttpHeaders({'Content-Type': 'application/json'})
};
@Injectable()
export class ApiserviceProvider {
  data: Observable<any>;
  
  constructor(
    public http: HttpClient, 
    public https: Http) {
  
  }
  //Declare API URL
  private listingApiUrl = 'http://42.1.63.244/api/createListing';
  private stateApiUrl= 'http://42.1.63.244/api/getState';
  public userloginUrl ='//42.1.63.244/api/userlogin?userid=';
  private listingtypeApiUrl = 'http://42.1.63.244/api/getListingType';
  private tenureApiUrl = 'http://42.1.63.244/api/getTenure';
  private titleApiUrl ='http://42.1.63.244/api/getTitle';
  private othersApiUrl = 'http://42.1.63.244/api/getOthers';
  private propertyTypeApiUrl = 'http://42.1.63.244/api/getPropertyType';
  private areaApiUrl = 'http://42.1.63.244/api/getArea';



  //POST Method Add Listing
  //assign a parameter name newListing that is called from Listing Model Class.
  addlisting(newListing: ListingModel): Observable<ListingModel>
  {
    /*return the http post methodwith the url, model class and the api header option 
      so that the method can add data in ionic into API using HTTP Post*/
    return this.http.post<ListingModel>(this.listingApiUrl,newListing,httpOptions).pipe(
      tap((listing: ListingModel) => console.log(`inserted listing= ${JSON.stringify(listing)}`)),
      catchError(error => of(new ListingModel()))
    );
  }


 
  //Get Method Login with M_User
  //pass the user & pass parameter so that it can return the value to other page that is needed to use this method.
  login(user,pass)
  {
    //Declare two variable that store user & pass
    var userinput = user;
    var userinput2 = pass;
    //using the two variable to make a security check if the two variable value is not null
    if(userinput != null && userinput2 != null)
    {
      /*using https get method to retrieve the api url and at the same time add in the two variable so that it can get the 
      exact data from the url.*/
      return this.https.get(this.userloginUrl + userinput + '&&p='+ userinput2);
    }  
  }
  

  //Get Method Display M_User Details
  //This Called Out Method is slow, because this function retrieving many data at once. (Suspect)
  //pass the user & pass parameter so that it can return the value to other page that is needed to use this method.
  DisplayUser(user,pass) 
  {
     //Declare two variable that store user & pass
    var userinput = user;
    var userinput2 = pass;
    /*using https get method to retrieve the api url and at the same time add in the two variable so that it can get the 
      exact data from the url.*/
    return this.https.get(this.userloginUrl + userinput+ '&&p=' + userinput2);
   
    
  }
  
  //Get Method Display Sponsor Details 
  //This Called Out Method is slow, because this function retrieving many data at once. (Suspect)
  DisplaySponsorDetail(user,pass)
  {
    var userinput = user;
    var userinput2 = pass;
    return this.https.get(this.userloginUrl + userinput + '&&p=' + userinput2);

  }

  //Get Method To Display the State Value For Drop Down Usage
  getState() 
  {
    //using https.get to retrieve the api url
    return this.https.get(this.stateApiUrl);
  }
  //Get Method To Display the Area Value For Drop Down Usage
  getArea()
  {
    //using https.get to retrieve the api url
    return this.https.get(this.areaApiUrl);
  }
 //Get Method To Display the Listing Type Value For Drop Down Usage
  getListingType()
  {
    //using https.get to retrieve the api url
    return this.https.get(this.listingtypeApiUrl);
  }
  //Get Method To Display the Tenure Value For Drop Down Usage
  getTenure()
  {
    //using https.get to retrieve the api url
    return this.https.get(this.tenureApiUrl);
  }
 //Get Method To Display the Title Value For Drop Down Usage
  getTitle()
  {
    //using https.get to retrieve the api url
    return this.https.get(this.titleApiUrl);
  }
 //Get Method To Display the Others Value For Drop Down Usage
  getOthers()
  {
    //using https.get to retrieve the api url
    return this.https.get(this.othersApiUrl);
  }

//Get Method To Display the Property Type Value For Drop Down Usage
  getPropertyType()
  {
    //using https.get to retrieve the api url
    return this.https.get(this.propertyTypeApiUrl);
  }

  //Get Method To Display the Listing Detail 
  getListingDetail()
  {
    //using https.get to retrieve the api url
    return this.https.get(this.listingApiUrl);
  }
}
