import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalesModalPage } from './sales-modal';

@NgModule({
  declarations: [
    SalesModalPage,
  ],
  imports: [
    IonicPageModule.forChild(SalesModalPage),
  ],
})
export class SalesModalPageModule {}
