import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import {HttpClient,HttpResponse,HttpRequest} from '@angular/common/http';
import 'rxjs';
import {Observable} from 'rxjs/Observable';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';
import { ToastController } from 'ionic-angular';
import {Http,Response} from '@angular/http';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  //Declare user and pass from ngModel
  user:any;
  pass:any;
  //Declare a form for validation
  loginForm: FormGroup;
  constructor(public navCtrl: NavController,
              public http: HttpClient,
              public ApiService: ApiserviceProvider, 
              private toastCtrl: ToastController,
              public https: Http,
              public formBuilder: FormBuilder  ) {

              //This validation occur if agent leaves the textbox blank, the button will not be clickable
              this.loginForm = formBuilder.group({
                'user': ['', Validators.compose([Validators.required])],
                'pass': ['',Validators.compose([Validators.required])]
              });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

//Login button 
loginHome()
  {
    /*Retreive user and pass value from APiService Provider to perform 1 stage of detection & retreiving the Api link 
     that contain user and pass info using Web Api*/
     //After retrieved the Api link, using subscribe to get the response from api and pass to json object.
    this.ApiService.login(this.user , this.pass).subscribe(
      (res:Response) => {
        //assign all the json object into a const to ready to called out the data.
        const checkLogin = res.json();
        //console.log(checkLogin); --> to check the value from api using console.log

        /*using the checkLogin const that contain the api value to futhter checking the UserID & Password from SQL
         with the value from the ngModel to make sure the data is exist*/
       if(checkLogin.u.UserID === this.user && checkLogin.u.Password === this.pass)
       {
        let toast = this.toastCtrl.create({
          message: 'Successfully login',
          duration: 2000,
          position: 'top'
        });
        toast.present();
    
        //redirect to home page and pass userid and pass.
        this.navCtrl.push(HomePage, {
          data: this.user,
          data1: this.pass
        });
       }

       else{
        let toast = this.toastCtrl.create({
          message: 'Invalid UserID or Password',
          duration: 2000,
          position: 'top'
      });
      toast.present();
  
       }
      });
        
       
      
    

    
  }

 
 
}
