import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DirectSponsorModalPage } from './direct-sponsor-modal';

@NgModule({
  declarations: [
    DirectSponsorModalPage,
  ],
  imports: [
    IonicPageModule.forChild(DirectSponsorModalPage),
  ],
})
export class DirectSponsorModalPageModule {}
