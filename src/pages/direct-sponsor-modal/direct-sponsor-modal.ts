import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';
import { Http,Response } from '@angular/http';
/**
 * Generated class for the DirectSponsorModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-direct-sponsor-modal',
  templateUrl: 'direct-sponsor-modal.html',
})
export class DirectSponsorModalPage {
//Declare user and pass to accept the value that comes from Sponsor Detail page
  user: string;
  pass: string;

  //Declare an array that store the sponsor data name result
  result: any[]=[];
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public view: ViewController,
              public ApiService: ApiserviceProvider) {

                //value comes from Sponsor Detail page.
                this.user = navParams.get('data');
                this.pass = navParams.get('data1');

                //Method called in onload to load the detail
                this.loadDirectSponsorDetail();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DirectSponsorModalPage');
  }

  //Refer api from APIService Provider
  // load the direct sponsor detail method
  loadDirectSponsorDetail()
  {
     //Retrieve DisplaySponsorDetail Method from ApiService Provider to execute the api.
    //Called the api service method in here and using subsribe method to get the api response.
    this.ApiService.DisplaySponsorDetail(this.user, this.pass)
    .subscribe((res: Response) => {
      //Declare a const name getSponsors to retrieve the api json object.
      const getSponsors = res.json();
      //Assign the followin data into an array name result that has declare above.
     this.result = getSponsors.u;

    })
  }

  //Close Modal Method
  closeModal()
  {
    //Dismiss the modal meaning that is close at the top left of the screen.
    this.view.dismiss();
  }

}
