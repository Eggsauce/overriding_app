import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiserviceProvider } from '../../providers/apiservice/apiservice';
import { Observable } from 'rxjs';
import {Response} from '@angular/http';
/**
 * Generated class for the DisplayListingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-display-listing',
  templateUrl: 'display-listing.html',
})


export class DisplayListingPage {

  //Declare a variable name listings that store the array of the json object from the api
  listings: any[] = [];
  //Declare a variable name listing
  listing: any;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private ApiService: ApiserviceProvider) {

                //Execute the loadListingDetail Method in OnLoad section.
                this.loadListingDetail();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DisplayListingPage');
  }

//Refer api from APIService Provider
// load the listing detail method
loadListingDetail()
{
  //Retrieve getListingDetail Method from ApiService Provider to execute the api.
  //Called the api service method in here and using subsribe method to get the api response.
  this.ApiService.getListingDetail().subscribe((res:Response) =>
  {
    //Declare a const name listing to retrieve the api json object.
    const listing = res.json();
    //Assign the following data into an array name result that has declare above.
    this.listings = listing;
  })
}


}
